//
// Created by Niklas on 31/07/2019.
//

#include "movie.h"

#include <utility>

movie::movie() {

    id = 0;
    title = "";
    length = 0.0;
}

movie::movie(int i, std::string s, float f) {

    id = i;
    title = std::move(s);
    length = f;
}

movie::movie(const movie& m) {

    try{
        id = m.id;
        title = m.title;
        length = m.length;
    } catch(std::exception exception){
        std::cout << exception.what()  << std::endl;
    }

}

movie::movie(const movie &&m) noexcept {

    try{
        id = m.id;
        title = m.title;
        length = m.length;
    } catch(std::exception exception){
        std::cout << exception.what()  << std::endl;
    }
}

movie &movie::operator=(const movie &m) const {

    try{
        movie tempMovie;
        tempMovie.id = m.id;
        tempMovie.title = m.title;
        tempMovie.length = m.length;

        return tempMovie;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

movie &movie::operator=(const movie &&m) noexcept {

    try{
        movie tempMovie;
        tempMovie.id = m.id;
        tempMovie.title = m.title;
        tempMovie.length = m.length;

        return tempMovie;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

bool movie::operator==(const movie& m) const{

    return this->id == m.id;
}

movie::~movie()= default;

std::shared_ptr<item> movie::clone() const {

    std::shared_ptr<item> iPtr1(new movie(*this));
    return iPtr1;
}

std::string movie::toString() {

    std::stringstream ss;
    ss << "Id : " << id << ", " << "Title : " << title << ", " << "Length : " << length << " minutes";
    std::string str = ss.str();
    return str;
}

int movie::getId() {

    return this->id;
}

std::string movie::getTitle() {

    return this->title;
}

std::string movie::getItemTag() {

    return this->itemTag;
}
