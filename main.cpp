//
// Created by Niklas on 31/07/2019.
//

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "book.h"
#include "movie.h"
#include "catalog.h"

template <class TYPE>
void print(TYPE data){

    std::cout << data << std::endl;
}
/**
 * Book-olion ja movie-olion toString()-funktiota testataan useasti, sillä vertailuoperaattori == vertaa ainoastaan
 * kahden item-olion id-numeroita.
 * */

TEST_CASE("BOOK",  "[book]"){

    book kirja = book(10, "Otsikko", "Nimi");
    CHECK(kirja.getId() == 10);
    CHECK(kirja.getTitle() == "Otsikko");
    CHECK(kirja.getItemTag() == "b");
    CHECK(kirja.toString() == "ID: 10, Title: Otsikko, Author: Nimi");

    book kirja1 = book(kirja);
    CHECK(kirja==kirja1);
    CHECK(kirja1.toString() == "ID: 10, Title: Otsikko, Author: Nimi");

    book kirja2 = book(std::move(kirja1));
    CHECK(kirja2.toString() == "ID: 10, Title: Otsikko, Author: Nimi");
    CHECK(kirja2==kirja);

    std::shared_ptr<item> testi = kirja.clone();
    CHECK(testi->toString() == kirja.toString());
}

TEST_CASE("MOVIE", "[movie]"){

    movie elokuva = movie(1, "James Bond", 127);
    CHECK(elokuva.getId() == 1);
    CHECK(elokuva.getItemTag() == "m");
    CHECK(elokuva.getTitle() == "James Bond");
    CHECK(elokuva.toString() == "Id : 1, Title : James Bond, Length : 127 minutes");

    movie elokuva1 = movie(elokuva);
    CHECK(elokuva1.toString() == "Id : 1, Title : James Bond, Length : 127 minutes");
    CHECK(elokuva == elokuva1);

    movie elokuva2 = movie(std::move(elokuva1));
    CHECK(elokuva==elokuva2);
    CHECK(elokuva2.toString() == "Id : 1, Title : James Bond, Length : 127 minutes");

    std::shared_ptr<item> testi = elokuva2.clone();
    CHECK(testi->toString() == elokuva2.toString());
}

TEST_CASE("CATALOG", "[catalog]"){

    movie elokuva = movie(1, "James Bond", 127);
    movie elokuva2 = movie(2, "Pokemon", 60);
    book kirja = book(10, "Harry Potter", "Rowling");
    book kirja2 = book(42, "Hitchhiker's etc.", "Adams");

    std::vector<std::shared_ptr<item>> lentItems;
    std::vector<std::shared_ptr<item>> itemsInStock;

    std::shared_ptr<item> iPtr1(new movie(elokuva));
    std::shared_ptr<item> iPtr2(new book(kirja));
    std::shared_ptr<item> iPtr3(new movie(elokuva2));
    std::shared_ptr<item> iPtr4(new book(kirja2));

    lentItems.push_back(iPtr1->clone());
    lentItems.push_back(iPtr2->clone());
    itemsInStock.push_back(iPtr3->clone());
    itemsInStock.push_back(iPtr4->clone());

    catalog katalogi = catalog(lentItems, itemsInStock);
    std::tuple<int, int> vectorSizes = katalogi.vectorSizes();
    //vectorSizes<0> = lentItems-vektorin pituus
    //vectorSizes<1> = itemsInStock-vektorin pituus

    CHECK(std::get<0>(vectorSizes) == 2);
    CHECK(std::get<1>(vectorSizes) == 2);

    katalogi.returnItem(lentItems[0]->clone());

    vectorSizes = katalogi.vectorSizes();
    CHECK(std::get<0>(vectorSizes) == 1);
    CHECK(std::get<1>(vectorSizes) == 3);

    katalogi.lendItem(itemsInStock[0]->clone());

    vectorSizes = katalogi.vectorSizes();
    CHECK(std::get<0>(vectorSizes) == 2);
    CHECK(std::get<1>(vectorSizes) == 2);

}

int main(int argc, char* argv[]){

    Catch::Session().run();

    movie elokuva1 = movie(1, "James Bond", 127);
    movie elokuva2 = movie(2, "Pokemon", 60);
    book kirja1 = book(10, "Harry Potter", "Rowling");
    book kirja2 = book(42, "Hitchhiker's etc.", "Adams");

    std::vector<std::shared_ptr<item>> lentItems;
    std::vector<std::shared_ptr<item>> itemsInStock;

    std::shared_ptr<item> iPtr1(new movie(elokuva1));
    std::shared_ptr<item> iPtr2(new book(kirja1));
    std::shared_ptr<item> iPtr3(new movie(elokuva2));
    std::shared_ptr<item> iPtr4(new book(kirja2));

    lentItems.push_back(iPtr1->clone());
    lentItems.push_back(iPtr2->clone());
    itemsInStock.push_back(iPtr3->clone());
    itemsInStock.push_back(iPtr4->clone());

    catalog katalogi = catalog(lentItems, itemsInStock);

    std::cout << "Tamanhetkinen katalogi : " << std::endl;
    print(katalogi.toString());

    std::string input = "";

    while (true) {

        std::cin.clear();
        //std::cin.ignore(std::numeric_limits<std::streamsize>::max());

        std::cout << "Ohjelman toiminnot : " << std::endl;
        std::cout << "1: Lisaa esine jarjestelmaan" << std::endl;
        std::cout << "2: Lainaa esine" << std::endl;
        std::cout << "3: Palauta esine" << std::endl;
        std::cout << "4: Tarkastele katalogia" << std::endl;
        std::cout << "5: Sulje ohjelma" << std::endl;

        std::cout << "Valitse toimenpide" << std::endl;
        std::cin >> input;

        if (input == "5") {
            std::cout << "Suljetaan ohjelma" << std::endl;
            return 0;
        } else if (input == "1") {

            try {
                std::string choice;
                std::stringstream ss;

                std::cout << "Valitse lisattava esine kirjoittamalla sita vastaava numero :" << std::endl;
                std::cout << "1: Kirja" << std::endl << "2: Elokuva " << std::endl;
                std::cin >> choice;

                if (choice == "1") {

                    int kirjaId;
                    std::string kirjaNimi;
                    std::string kirjaKirjoittaja;

                    std::cout << "Esita lisattavan kirjan tiedot jarjestyksessa id, kirjan nimi, kirjan kirjoittaja"
                              << std::endl;
                    std::cout << "Syota ID" << std::endl;

                    std::cin >> kirjaId;

                    if(std::cin.fail()){
                        throw std::invalid_argument("Seuraa ohjeita tarkasti!");
                    }

                    else{
                        std::cout << "Syota kirjan nimi" << std::endl;
                        std::cin >> kirjaNimi;
                        std::cout << "Syota kirjan kirjoittaja" << std::endl;
                        std::cin >> kirjaKirjoittaja;

                        book tempKirja = book(kirjaId, kirjaNimi, kirjaKirjoittaja);
                        std::shared_ptr<item> tempPtr(new book(tempKirja));
                        katalogi.addItem(tempPtr->clone());
                        std::cout << "Kirja lisatty varastoon" << std::endl;
                        choice = "";
                    }
                } else if (choice == "2") {
                    int elokuvaId;
                    std::string elokuvaNimi;
                    float elokuvaKesto;

                    std::cout << "Esita lisattavan elokuvan tiedot jarjestyksessa id, elokuvan nimi, elokuvan kesto"
                              << std::endl;
                    std::cout << "Syota ID" << std::endl;

                    std::cin >> elokuvaId;

                    if(std::cin.fail()){
                        throw std::invalid_argument("Seuraa ohjeita tarkasti!");
                    }

                    else{
                        std::cout << "Syota elokuvan nimi" << std::endl;
                        std::cin >> elokuvaNimi;
                        std::cout << "Syota elokuvan kesto" << std::endl;
                        std::cin >> elokuvaKesto;

                        movie tempElokuva = movie(elokuvaId, elokuvaNimi, elokuvaKesto);
                        std::shared_ptr<item> tempPtr(new movie(tempElokuva));
                        katalogi.addItem(tempPtr->clone());
                        std::cout << "Elokuva lisatty varastoon" << std::endl;
                        choice = "";
                    }
                } else {
                    throw std::invalid_argument("Valitse 1 tai 2 ");
                }
            } catch (std::exception e) {
                std::cout << "Seuraa ohjeita tietoja syottaessa" << std::endl;
            }
            std::cin.clear();
        } else if (input == "2") {

            int testi1 = std::get<1>(katalogi.vectorSizes());
            if(testi1==0){
                std::cout << "Esineita ei varastossa" << std::endl;
            }
            else{
                std::string tempStr1 = katalogi.getItemsInStock()[testi1-1]->toString();
                std::stringstream ss1;

                katalogi.lendItem(katalogi.getItemsInStock()[testi1-1]->clone());

                if(testi1 == std::get<1>(katalogi.vectorSizes())){
                    std::cout << "Jotain meni pieleen";
                }
                else{
                    ss1 << "Lainattiin esine " << tempStr1 << std::endl;
                }
                tempStr1 = "";
                std::string tempStr2 = ss1.str();
                std::cout << tempStr2 << std::endl;
            }

        } else if (input == "3") {

            int testi2 = std::get<0>(katalogi.vectorSizes());
            if(testi2==0){
                std::cout << "Esineita ei lainassa" << std::endl;
            }
            else{
                std::string tempStr3 = katalogi.getLentItems()[testi2-1]->toString();
                std::stringstream ss2;

                katalogi.returnItem(katalogi.getLentItems()[testi2-1]->clone());

                if(testi2 == std::get<0>(katalogi.vectorSizes())){
                    std::cout << "Jotain meni pieleen";
                }
                else{
                    ss2 << "Palautettiin esine " << tempStr3 << std::endl;
                }
                tempStr3 = "";

                std::string tempStr4 = ss2.str();
                std::cout << tempStr4 << std::endl;
                tempStr4 = "";
            }

        } else if (input == "4") {
            print(katalogi.toString());
        } else {
            std::cout << "Valitse vaihtoehto valilta 1-5" << std::endl;
            continue;
        }
        std::cin.clear();
    }
}