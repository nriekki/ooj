//
// Created by Niklas on 31/07/2019.
//

#include "book.h"

book::book() {

    id = 0;
    title = "";
    author = "";
}

book::book(int i, std::string t, std::string a) {

    id = i;
    title = t;
    author = a;
}

book::book(const book &b) {

    try{
        this->id = b.id;
        this->title = b.title;
        this->author = b.author;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

book::book(const book &&b) noexcept {

    try{
        this->id = b.id;
        this->title = b.title;
        this->author = b.author;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

book &book::operator=(const book &b) const {

    try{
        book tempBook;
        tempBook.id = b.id;
        tempBook.title = b.id;
        tempBook.author = b.author;

        return tempBook;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

book &book::operator=(const book &&b) noexcept{

    try{
        book tempBook;
        tempBook.id = b.id;
        tempBook.title = b.title;
        tempBook.author = b.author;

        return tempBook;
    } catch(std::exception exception){
        std::cout << exception.what() << std::endl;
    }
}

bool book::operator==(const book& b) const{

    return this->id == b.id;
}

book::~book()= default;

std::shared_ptr<item> book::clone() const {

    std::shared_ptr<item> uPtr(new book(this->id, this->title, this->author));
    std::cout << uPtr->toString() << std::endl;
    return uPtr;
}

std::string book::toString() {

    std::stringstream ss;
    ss << "ID: " <<  id  << ", Title: " <<  title << ", Author: " << author;
    std::string text = ss.str();
    return text;
}

int book::getId() {

    return this->id;
}

std::string book::getTitle() {

    return this->title;
}

std::string book::getItemTag() {

    return this->itemTag;
}
