//
// Created by Niklas on 31/07/2019.
//

#ifndef UNTITLED1_BOOK_H
#define UNTITLED1_BOOK_H

#include "item.h"

class book : public item {
/**
 * @class book
 * @brief item-luokan alaluokka book. Muodostaa järjestelmään olioita kirjoista. Sisältää erilaisia konstruktoreita ja
 * operaattoreita. Luokassa myös muutamia gettereitä testauksia ja muiden luokkien operaatioita varten. Gettereiden ansioista
 * book-olion yksityiset arvot pystytään pitämään yksityisinä, ja datan enkapsulaatio säilyy.
* */

private:

    int id;
    std::string title;
    std::string author;
    std::string itemTag = "b";

public:

    /** @brief Oletuskonstruktori book-oliolle ilman arvoja **/
    book();

    /**
     * @brief Konstruktori arvojen kanssa
     * @param i int joka toimii kirjan tunnistenumerona
     * @param s1 string jossa kirjan nimi
     * @param s2 string jossa kirjan kirjoittaja
     * @return annetuilla arvoilla alustettu book-olio*/
    book(int i, std::string s1, std::string s2);

    /**
     * @brief konstruktori book-olion kopiolla
     * @param b book-olio joka kopioidaan
     * @return kopio argumenttina annetusta book-oliosta
     */
    book(const book& b);

    /**
     * @brief book-olion siirtokopio
     * @param b book-olio joka siirretään ja kopioidaan
     * @return siirtokopio argumenttina annetusta book-oliosta
     */
    book(const book&& b) noexcept;

    /**
     * @brief sijoitusoperaattori book-olion kopiolla, sijoittaa kopion paratmetrinä annetusta book-oliosta
     * @param b book-olio josta kopio tehdään
     * @return book-olio joka on kopio parametrinä annetusta book-oliosta
     */
    book& operator=(const book &b) const;

    /**
     * @brief sijoitusoperaattori book-olion siirtokopiolla, sijoittaa rvaluena annetun kirjan kopion book-olioon
     * @param b book-olio josta siirtokopio tehdään
     * @return book-olio joka on siirtokopio parametrinä annetusta book-oliosta
     */
    book& operator=(const book&& b) noexcept;

    /**
     * @brief vertailuoperaattori book-oliolle. Vertaa kahden book-olion id:tä.
     * @param b book-olio johon kutsutun book-olion id:tä verrataan
     * @return palauttaa true jos id sama, false muussa tapauksessa
     */
    bool operator==(const book& b) const;

    ~book() override;

    /**
     * @brief book-olion kloonaus
     * @return kopio book-oliosta
     */
    std::shared_ptr<item> clone() const override;

    /**
     * @brief Book-olion sisällön esitys stringinä
     * @return string book-olion sisältämistä arvoista
     */
    std::string toString() override;

    int getId() override;

    std::string getTitle() override;

    std::string getItemTag() override;
};
#endif //UNTITLED1_BOOK_H
