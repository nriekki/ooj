//
// Created by Niklas on 06/08/2019.
//

#ifndef UNTITLED1_CATALOG_H
#define UNTITLED1_CATALOG_H

#include "item.h"
#include <algorithm>
#include <vector>
#include <cctype>

class catalog{
/**
 * @class catalog
 * @brief catalog-luokka toimii item-olioiden katalogina, ja kertoo mitkä item-oliot
 * ovat lainassa ja varastossa
 */
private:

    /**
     * Yksityiset parametrit:
     * @param itemsInStock, vektori älykkäitä osoittimia niihin item-olioihin, jotka ovat varastossa
     * @param lentItems, vektori älykkäitä osoittimia niihin item-olioihin, jotka ovat lainassa
     */
    std::vector<std::shared_ptr<item>> ItemsInStock;
    std::vector<std::shared_ptr<item>> lentItems;

public:

    /**
     * @brief catalog-olion alustukset.
     * Ensimmäinen funktio alustaa tyhjän catalog-olion,
     * toinen funktio alustaa catalog-olion parametreinä annetuilla vektoreilla.
     */

    catalog();

    /**
     * @brief catalog-olion alustus kahdella vektorilla
     * @param v1, vektori joka sisältää pointereita item-olioihin. Muodostaa lainattujen item-olioiden vektorin
     * @param v2, vektori joka sisältää pointereita item-olioihin. Muodostaa varastossa olevien item-olioiden vektorin
     * */
    catalog(std::vector<std::shared_ptr<item>> lentVector, std::vector<std::shared_ptr<item>> stockVector);

    /**
     * @brief funktio palauttaa catalog-olion lainattujen esineiden vektorin, käytetään
     * mm. vektorin koon tarkastelussa
     * @return catalog-olion lentItems-vektori
     * */
    std::vector<std::shared_ptr<item>> getLentItems();

    /**
    * @brief funktio palauttaa catalog-olion varastossa olevien esineiden vektorin, käytetään
    * mm. vektorin koon tarkastelussa
    * @return catalog-olion itemsInStock-vektori
    * */
    std::vector<std::shared_ptr<item>> getItemsInStock();

    /**
     * @brief funktio item-olion lainaamiseen. Lisää parametrinä annetun pointerin osoittamaan
     * item-olioon lainattujen vektoriin, ja poistaa sen varastossa olevien vektorista
     * @param iPtr, osoitin item-olioon, joka lainataan
     * */
    void lendItem(std::shared_ptr<item> iPtr);

    /**
     * @brief funktio item-olion palauttamiseen. Lisää parametrinä annetun pointerin osoittamaan
     * item-olioon varastossa olevien vektoriin, ja poistaa sen lainattujen vektorista
     * @param iPtr, osoitin item-olioon, joka palautetaan
     * */
    void returnItem(std::shared_ptr<item> iPtr);

    /**
     * @brief Vektoreiden sisällön tulostamiselle ja niiden yhdistämiselle on omat funktiot niiden toteutuksen luettavuuden
     * säilytyksen nimissä.
     * lentToString-funktio palauttaa stringin joka kertoo lainassa olevien kirjojen ja elokuvien nimet
     * @return string jossa listattuna lainassa olevat kirjat ja elokuvat
     */

    /**
     * @brief Funktio lisää item-olion seurannan ulkopuolelta järjestelmään.
     * */
    void addItem(std::shared_ptr<item> iPtr);

    /**
     * @brief funktio poistaa item-olion järjestelmän seurannasta*/
    void removeItem(std::shared_ptr<item> iPtr);

    std::string lentToString();

    /**
     * @brief funktio muodostaa stringin varastossa olevien kirjojen ja elokuvien nimistä
     * @return string jossa varastossa olevat kirjat ja elokuvat
     */
    std::string stockToString();

    /**
     * @brief toString-funktio yhdistää kahden edellisen funktion palauttamat stringit, ja tulostaa sekä lainassa olevien
     * että varastossa olevien elokuvien ja kirjojen nimet
     * @returnstring jossa kaikki seurannassa olevien item-olioiden nimet*/
    std::string toString();

    /**
     * @brief funktio catalog-olion vektoreiden koon tutkimiselle. Mahdollistaa kokojen tarkistamisen
     * pitäen itse vektorit yksityisinä muuttujina. Käytetään ainoastaan testauksessa.
     * @return palauttaa molempien vektoreiden pituudet tuplena */
    std::tuple<int, int> vectorSizes();

};

#endif //UNTITLED1_CATALOG_H
