//
// Created by Niklas on 31/07/2019.
//

#ifndef UNTITLED1_MOVIE_H
#define UNTITLED1_MOVIE_H

#include "item.h"

class movie : public item {
/**
 * @class movie
 * @brief item-luokan alaluokka movie. Muodostaa järjestelmään olioita elokuvista. Sisältää erilaisia konstruktoreita ja
 * operaattoreita. Luokassa myös muutamia gettereitä testauksia ja muiden luokkien operaatioita varten. Gettereiden ansioista
 * movie-olion yksityiset arvot pystytään pitämään yksityisinä, ja datan enkapsulaatio säilyy.
* */

private:

    int id;
    std::string title;
    float length;
    std::string itemTag = "m";

public:
    /**
     * @brief oletuskonstruktori movie-oliolle
     * @return palauttaa tyhjän movie-olion
     */
    movie();
    /**
     * @brief konstruktori movie-oliolle parametrinä annetuilla arvoilla
     * @param i int joka kertoo movie-olion id-arvon
     * @param s string jossa elokuvan nimi
     * @param f float jossa elokuvan kesto
     * @return palauttaa alustetun movie-olion
     */
    movie(int i, std::string s, float f);

    /**
     * @brief konstruktori movie-olion kopiolla
     * @param m movie-olio joka kopioidaan
     * @return kopio argumenttina annetusta movie-oliosta
     */
    movie(const movie& m);
    /**
     * @brief konstruktori movie-olion siirtokopiolla
     * @param m movie-olio joka siirretään ja kopioidaan
     * @return siirtokopio movie-oliosta
     */
    movie(const movie&& m) noexcept;

    /**
     * @brief sijoitusoperaattori movie-olion kopiolla, sijoittaa kopion paratmetrinä annetusta movie-oliosta
     * @param m movie-olio josta kopio tehdään
     * @return movie-olio joka on kopio parametrinä annetusta movie-oliosta
     */
    movie& operator=(const movie &m) const;

    /**
     * @brief sijoitusoperaattori movie-olion siirtokopiolla, sijoittaa rvaluena annetun kirjan kopion movie-olioon
     * @param m movie-olio josta siirtokopio tehdään
     * @return movie-olio joka on siirtokopio parametrinä annetusta movie-oliosta
     */
    movie& operator=(const movie&& m) noexcept;

    /**
     * @brief vertailuoperaattori movie-oliolle. Vertaa kahden movie-olion id:tä.
     * @param m movie-olio johon kutsutun movie-olion id:tä verrataan
     * @return palauttaa true jos id sama, false muussa tapauksessa
     */
    bool operator==(const movie& m) const;

    ~movie() override;

    /**
     * @brief movie-olion kloonaus
     * @return kopio movie-oliosta
     */
    std::shared_ptr<item> clone() const override;

    /**
     * @brief movie-olion sisällön esitys string-muodossa
     * @return string movie-olion sisältämistä arvoista
     */
    std::string toString() override;

    /**
     * @brief funktio id:n noutamiselle. Käytetään esimerkiksi movie-olion vertailuoperaattorissa, ja catalog-olion
     * toimenpiteissä
     * @return yhden movie-olion id
     */
    int getId() override;

    std::string getTitle() override;

    std::string getItemTag() override;
};

#endif //UNTITLED1_MOVIE_H