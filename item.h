//
// Created by Niklas on 31/07/2019.
//

#ifndef UNTITLED1_ITEM_H
#define UNTITLED1_ITEM_H

#include <string>
#include <memory>
#include <sstream>
#include <iostream>

class item {

public:

    virtual ~item();
    virtual std::shared_ptr<item> clone() const = 0;
    virtual std::string toString() = 0;
    virtual int getId() = 0;
    virtual std::string getTitle() = 0;
    virtual std::string getItemTag() = 0;
};

#endif //UNTITLED1_ITEM_H
