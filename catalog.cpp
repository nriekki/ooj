//
// Created by Niklas on 25/11/2019.
//

#include "catalog.h"

//Tarviiko?
catalog::catalog() {

}

catalog::catalog(std::vector<std::shared_ptr<item>> v1, std::vector<std::shared_ptr<item>> v2) {

    this->lentItems = v1;
    this->ItemsInStock = v2;
}

std::vector<std::shared_ptr<item>> catalog::getLentItems() {
    return this->lentItems;
}

std::vector<std::shared_ptr<item>> catalog::getItemsInStock() {
    return this->ItemsInStock;
}

void catalog::lendItem(std::shared_ptr<item> iPtr) {

    for(int i = 0; i < this->ItemsInStock.size(); i++){

        if(iPtr->getId() == this->ItemsInStock[i]->getId()){

            this->lentItems.push_back(iPtr->clone());
            this->ItemsInStock.erase(ItemsInStock.begin() + i);
            i = this->lentItems.size();
            return;
        }
    }
    std::cout << "Esinetta ei varastossa" << std::endl;
}

void catalog::returnItem(std::shared_ptr<item> iPtr) {

    for(int i = 0; i < this->lentItems.size(); i++){

        if(iPtr->getId() == this->lentItems[i]->getId()){

            this->ItemsInStock.push_back(iPtr->clone());
            this->lentItems.erase(lentItems.begin() + i);
            i = this->lentItems.size();
            return;
        }
    }
    std::cout << "Esine ei lainassa" << std::endl;
}

void catalog::addItem(std::shared_ptr<item> iPtr) {

    this->ItemsInStock.push_back(iPtr);
}

void catalog::removeItem(std::shared_ptr<item> iPtr) {

    for (int i = 0; i < this->ItemsInStock.size(); i++){
        if(iPtr->getId() == this->ItemsInStock[i]->getId()){
            this->ItemsInStock.erase(ItemsInStock.begin() + i);
            return;
        }
    }
    std::cout << "Esinetta ei loytynyt" << std::endl;
}

std::string catalog::stockToString() {

    std::stringstream printStream;
    std::vector<std::string> bookVector;
    std::vector<std::string> movieVector;

    printStream << "VARASTOSSA OLEVAT KIRJAT : " << std::endl;
    for(int i = 0; i < this->ItemsInStock.size(); i++){

        if(this->ItemsInStock[i]->getItemTag() == "b"){
            bookVector.push_back(this->ItemsInStock[i]->getTitle());
        } else {
            movieVector.push_back(this->ItemsInStock[i]->getTitle());
        }
    }

    for(int j = 0; j < bookVector.size(); j++){
        printStream << bookVector[j] << ", ";
    }

    printStream << std::endl << "VARASTOSSA OLEVAT ELOKUVAT :" << std::endl;

    for(int k = 0; k < movieVector.size(); k++){
        printStream << movieVector[k] << ", ";
    }

    printStream << std::endl;
    std::string text = printStream.str();
    return text;
}

std::string catalog::lentToString(){

    std::stringstream printStream;
    std::vector<std::string> bookVector;
    std::vector<std::string> movieVector;

    printStream << "LAINASSA OLEVAT KIRJAT : " << std::endl;
    for(int i = 0; i < this->lentItems.size(); i++){

        if(this->lentItems[i]->getItemTag() == "b"){
            bookVector.push_back(this->lentItems[i]->getTitle());
        } else {
            movieVector.push_back(this->lentItems[i]->getTitle());
        }
    }

    for(int j = 0; j < bookVector.size(); j++){
        printStream << bookVector[j] << ", ";
    }

    printStream << std::endl << "LAINASSA OLEVAT ELOKUVAT :" << std::endl;

    for(int k = 0; k < movieVector.size(); k++){
        printStream << movieVector[k] << ", ";
    }

    printStream << std::endl;
    std::string text = printStream.str();
    return text;
}

std::string catalog::toString() {

    std::stringstream printStream;
    printStream << this->lentToString() << this->stockToString();

    return printStream.str();
}

std::tuple<int, int> catalog::vectorSizes() {

    return std::make_tuple(this->lentItems.size(), this->ItemsInStock.size());
}